const fs = require('fs');
const util = require('util');
const fpath = './logs.log';
let DEV = process.env.NODE_ENV === 'development';

exports.logger = function(){
    let date = new Date;
    let timestamp = date.getFullYear() + padZeros(date.getMonth() + 1, 2) + padZeros(date.getDate(), 2) + " ";
    timestamp += padZeros(date.getHours(), 2) + ":" + padZeros(date.getMinutes(), 2) + ":" + padZeros(date.getSeconds(), 2);
    timestamp += "." + padZeros(date.getMilliseconds(), 3);
    if ( DEV ){
        console.log(timestamp+' '+util.format.apply(null,arguments));
    }
    fs.appendFile(fpath,timestamp+' '+util.format.apply(null,arguments)+'\n',function (err) {
        if (err) throw err;
        fs.stat(fpath,function (err,stat) {
            if (err) throw err;
            if ( stat.size > 1024*1024*1024 )  // 1 megabytes
                fs.rename(fpath,fpath+'.1',function () {
                    if (err) throw err;
                });
        });
    });
};

function padZeros (num, digits) {
      let zerosToAdd;
      num = String(num);
      zerosToAdd = digits - num.length;
      while (zerosToAdd > 0) {
        num = '0' + num;
        zerosToAdd = zerosToAdd - 1;
      }
      return num;
}
